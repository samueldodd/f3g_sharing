  with
  lags as
  (
  select
  pm_user_id,
  platform_account_id,
  game_account_id,
  internal_price,
  catalog_type,
  purchased_at,
  payment_no,
  first_value(internal_price) over (
  partition by pm_user_id
  order by payment_no
  rows between unbounded preceding and current row
  ) as initial_price,
  first_value(catalog_type) over (
  partition by pm_user_id
  order by payment_no
  rows between unbounded preceding and current row
  ) as initial_catalog,
  lag(purchased_at) over (
  partition by pm_user_id
  order by purchased_at ) as last_purchase,
  lag(internal_price) over (
  partition by pm_user_id
  order by purchased_at ) as last_package,
  case
  when country in ('AU','US') then country
  else 'OTHER'
  end as region


  from pm.f3g_payments --CHANGE THIS TABLE TO QUERY EACH INDIVIDUAL GAME AS DESIRED
  )
  select
  pm_user_id,
  game_account_id,
  initial_price,
  initial_catalog,
  payment_no,
  internal_price,
  last_package,
  catalog_type,
  purchased_at,
  region,
  datediff(hour, last_purchase, purchased_at) as hour_lag,
  datediff(day, last_purchase, purchased_at) as day_lag,
  datediff(min, last_purchase, purchased_at) as min_lag,
  datediff(sec, last_purchase, purchased_at) as sec_lag

  from lags
  where payment_no > 1 and last_purchase != '1970-01-01'
  and catalog_type != '' and initial_catalog != ''
  order by lags.pm_user_id, purchased_at
  ;
